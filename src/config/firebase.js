import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyDmJfUjzFkZNqdCZdEbB6-i2XBjhWLAIUA",
  authDomain: "chouseifirebase.firebaseapp.com",
  databaseURL: "https://chouseifirebase.firebaseio.com",
  projectId: "chouseifirebase",
  storageBucket: "chouseifirebase.appspot.com",
  messagingSenderId: "943032540261",
  appId: "1:943032540261:web:8236328d61a998032fba8c"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
